\select@language {polish}
\contentsline {chapter}{Wykaz skr\IeC {\'o}t\IeC {\'o}w i oznacze\IeC {\'n}}{3}{chapter*.1}
\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Wprowadzenie do aktywnej redukcji ha\IeC {\l }asu}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Cel, za\IeC {\l }o\IeC {\.z}enia i zakres pracy}{5}{section.1.2}
\contentsline {chapter}{\numberline {2}Przegl\IeC {\k a}d rozwi\IeC {\k a}za\IeC {\'n}}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Systemy aktywnej redukcji ha\IeC {\l }asu w ochronnikach s\IeC {\l }uchu}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Struktury sterownik\IeC {\'o}w w systemach aktywnej redukcji ha\IeC {\l }asu}{7}{section.2.2}
\contentsline {section}{\numberline {2.3}Algorytmy heurystyczne w aktywnej redukcji ha\IeC {\l }asu}{7}{section.2.3}
\contentsline {chapter}{\numberline {3}Projekt symulacji systemu ANC opartego o algorytm ewolucyjny}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Ewolucyjna strategia adaptacji macierzy kowariancji}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Wykorzystanie algorytmu ewolucyjnego w systemie ANC}{9}{section.3.2}
\contentsline {section}{\numberline {3.3}G\IeC {\l }\IeC {\'o}wne zagadnienia symulacyjne i spos\IeC {\'o}b ich rozwi\IeC {\k a}zania}{9}{section.3.3}
\contentsline {section}{\numberline {3.4}Zakres i parametry symulacji}{9}{section.3.4}
\contentsline {section}{\numberline {3.5}Narz\IeC {\k e}dzia i szczeg\IeC {\'o}\IeC {\l }y implementacji systemu ANC}{9}{section.3.5}
\contentsline {chapter}{\numberline {4}Symulacje systemu ANC opartego o algorytm ewolucyjny}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Przebieg pomiar\IeC {\'o}w}{11}{section.4.1}
\contentsline {section}{\numberline {4.2}Wyniki pomiar\IeC {\'o}w z komentarzem}{11}{section.4.2}
\contentsline {section}{\numberline {4.3}Ocena rozwi\IeC {\k a}zania}{11}{section.4.3}
\contentsline {chapter}{\numberline {5}Wnioski i podsumowanie}{13}{chapter.5}
\contentsline {chapter}{Bibliografia}{15}{chapter*.2}
\contentsline {chapter}{Dodatek \numberline {A}Fragmenty kodu wykorzystywanego przy symulacjach}{17}{appendix.A}
\contentsline {chapter}{Dodatek \numberline {A}Fragmenty kodu zaimplementowanego w prototypie}{19}{appendix.A}
\contentsline {chapter}{Dodatek \numberline {A}Pe\IeC {\l }ne wyniki pomiar\IeC {\'o}w weryfikacyjnych}{21}{appendix.A}
