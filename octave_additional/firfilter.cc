#include <octave/oct.h>

DEFUN_DLD(firfilter, args, ,
          "Filters given signal x with the given filter coeffitients h.")
{
    static int x_length;
    static int h_length;
    static double sum;

    RowVector x(args(0).matrix_value());   
    RowVector h(args(1).matrix_value());

    x_length = x.length();
    h_length = h.length(); 
    sum = 0;

    for(int i=0; i<h_length; i++) {
	sum = sum + h(i)*x(x_length-1-i);
    }

    return octave_value(sum);            
}
