def do(R,function):
    L_p = len(R);
    L_g = len(R[0]);
    p = range(0,L_p,1)
    minimum = 0;
    eta = 0.05;

    for i in range(0,L_p,1):
        p[i] = function(R[i]);
    minimum = min(p);
    
    if minimum < 0:
        C1 = - minimum + eta;
        for i in range(0,L_p,1):
            p[i] = p[i] + C1;
    return p

def test():
    Rs = range(0,2,1)
    Rs[0] = [0,0,0,0,0,0,0,0,0,0]
    Rs[1] = [1,1,1,1,1,1,1,1,1,1]
    from ..objective import sum_of_ones
    print do(Rs,sum_of_ones.do)
    #~ bitflop.bitflop(Rs)
    #~ print Rs

if __name__ == '__main__':
  test()
