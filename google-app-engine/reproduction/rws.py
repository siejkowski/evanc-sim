from random import random

def do(Rp,p):
    L_p = len(p)
    n = len(Rp[0])
    suma = float(sum(p))
    #~ print p
    #~ print suma
    Rs = [[] for x in range(0,len(Rp))]
    rozklad = [i/suma for i in p]
    #~ print rozklad
    dystrybuanta = range(0,len(rozklad))
    dystrybuanta[0] = rozklad[0]
    temp = 0
    for i in range(1,L_p,1):
        dystrybuanta[i] = dystrybuanta[i-1] + rozklad[i]
    for i in range(0,L_p,1):
        alpha = random()
        #~ print alpha
        #~ print dystrybuanta
        #~ print rozklad
        #~ print [dystrybuanta.index(x) for x in dystrybuanta if x>=alpha]
        j = [dystrybuanta.index(x) for x in dystrybuanta if x>=alpha]
        #~ if j == []:
            #~ Rs[i][0:n] = Rp[0][0:n]
        #~ else:
        Rs[i][0:n] = Rp[min(j)][0:n]
    return Rs

def test():
    Rs = range(0,2,1)
    p = [0.5, 0.5]
    Rs[0] = [0,0,0,0,0,0,0,0,0,0]
    Rs[1] = [1,1,1,1,1,1,1,1,1,1]
    print do(Rs,p)

if __name__ == '__main__':
  test()
