from random import random

def do(L_p=20,L_z=12,dk=64,pg=0.5):
	L_g = L_z*dk; # liczba genow na osobnika
	R = [range(L_g) for x in range(L_p)] # macierz populacji
	for i in range(0,L_p,1):
		for j in range(0,L_g,1):
			if random() <= pg:
				R[i][j] = 0;
			else:
				R[i][j] = 1;
	return R

def test():
    print random_population(10,5,1)

if __name__ == '__main__':
  test()
