from random import random, shuffle
from math import floor

#~ def onepoint(Rp,pc = 0.9):  
    #~ L_p = len(Rp) 
    #~ n = len(Rp[0])
    #~ Rs = [range(0,n) for x in range(0,L_p)]
    #~ shuffle(Rp)
    #~ for i in range(0,L_p,2):
        #~ Rs[i][0:n] = Rp[i][0:n]
        #~ Rs[i+1][0:n] = Rp[i+1][0:n]
        #~ if pc > random():
            #~ cp = int(floor(n*random()))
            #~ Rs[i][cp:n] = Rp[i+1][cp:n]
            #~ Rs[i+1][cp:n] = Rp[i][cp:n]
    #~ return Rs

def do(Rp,pc = 0.9):  
    L_p = len(Rp) 
    n = len(Rp[0])
    shuffle(Rp)
    slice_copy = range(n)
    for i in range(0,L_p,2):
        if pc > random():
            cp = int(floor(n*random()))
            slice_copy[cp:n] = Rp[i][cp:n]
            Rp[i][cp:n] = Rp[i+1][cp:n]
            Rp[i+1][cp:n] = slice_copy[cp:n]

def test():
    Rs = range(0,4,1)
    Rs[0] = [0,0,0,0,0,0,0,0,0,0]
    Rs[1] = [1,1,1,1,1,1,1,1,1,1]
    Rs[2] = [0,0,0,0,0,0,0,0,0,0]
    Rs[3] = [1,1,1,1,1,1,1,1,1,1]
    onepoint(Rs)
    print Rs

if __name__ == '__main__':
  test()
