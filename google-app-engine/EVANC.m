clear all;
close all;
%[s,m,b] = urlread("http://evolutionaryanc.appspot.com/run","post",[{"M","800"},{"LP","100"},{"LW","8"},{"pc","0.999"},{"pm","0.001"},{"bits","12"},{"limit","1.0"},{"signal_size","1000"},{"fp","44100"}])

%[s,m,b] = urlread("http://evolutionaryanc.appspot.com/run")

M = 10

[s,m,b] = urlread("http://localhost:8080/init","post",[{"M",num2str(M)},{"LP","100"},{"LW","8"},{"pc","0.999"},{"pm","0.001"},{"bits","12"},{"limit","1.0"},{"signal_size","1000"},{"fp","44100"}])

for i = 1:1:(M+1)
	[s,m,b] = urlread("http://localhost:8080/run","post",[{"gen",num2str(i-1)}]);
	res = strsplit(s,";");
	cell2mat(res(1,1))
	p_sr(i) = str2num(cell2mat(strsplit(cell2mat(res(1,2)),"=")(1,2)));
	p_best(i) = str2num(cell2mat(strsplit(cell2mat(res(1,3)),"=")(1,2)));
	p_worst(i) = str2num(cell2mat(strsplit(cell2mat(res(1,4)),"=")(1,2)));
	tictoc(i) = str2num(cell2mat(strsplit(cell2mat(res(1,5)),"=")(1,2)));
	w_best(i,:) = str2num(cell2mat(strsplit(cell2mat(res(1,6)),"=")(1,2)));
endfor
hold on
plot(p_sr)
plot(p_best)
plot(p_worst)
hold off
%res = strsplit(s,";")
%
%for i = 1:1:length(res)
	%strsplit(res(i),"=")
%endfor