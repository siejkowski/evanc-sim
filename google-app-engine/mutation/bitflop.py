from random import random
from math import floor

def do(Rp, pm = 0.5):
    L_p = len(Rp);
    n = len(Rp[0]);  
    for i in range(0,L_p,1):
        for j in range(0,n,1):
            alpha = random()
            if pm > alpha:
                if Rp[i][j] == 0:
                    Rp[i][j] = 1
                else:
                    Rp[i][j] = 0

def test():
    Rs = range(0,2,1)
    Rs[0] = [0,0,0,0,0,0,0,0,0,0]
    Rs[1] = [1,1,1,1,1,1,1,1,1,1]
    bitflop(Rs,0.5)
    print Rs

if __name__ == '__main__':
  test()
