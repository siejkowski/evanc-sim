import os
from mapreduce import base_handler
from mapreduce import mapreduce_pipeline
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

class TestMapReduce(base_handler.PipelineBase):
    def run(self, filekey, blobkey):
        print "run!"
        output = yield mapreduce_pipeline.MapreducePipeline(
                                                            "change_the_object",
                                                            "TestMapReduce.map_step",
                                                            "TestMapReduce.reduce_step",
                                                            "mapreduce.input_readers.DatastoreInputReader",
                                                            "mapreduce.output_writers.BlobstoreOutputWriter",
                                                            mapper_params={
                                                                           "entity_kind": "test_table",
                                                                           "namespaces" : namespac,
                                                                           },
                                                            reducer_params={
                                                                            "mime_type": "text/plain",
                                                                            },
                                                            shards=16)
    def map_step(self):
        print "map step!"
        
    def reduce_step(self):
        print "reduce step!"

class DoneHandler(webapp.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('Done! MapReduce is done! OMG!')
        
class WaitHandler(webapp.RequestHandler):
    def get(self):
        pipeline_id = self.request.get('pipeline')
        pipeline = mapreduce_pipeline.MapreducePipeline.from_id(pipeline_id)
        if pipeline.has_finalized:
            self.redirect('/done')
        else:
            self.redirect('/wait?pipeline=' + pipeline.pipeline_id)
        
class MainHandler(webapp.RequestHandler):
    def get(self):
        pipeline = mapreduce_pipeline.MapreducePipeline(TestMapReduce())
        pipeline.start()
        self.redirect('/wait?pipeline=' + pipeline.pipeline_id)

application = webapp.WSGIApplication(
[('/.*', MainHandler), ('/wait', WaitHandler), ('/done', DoneHandler)]
, debug=True)

def main():
    run_wsgi_app(application)

if __name__ == '__main__':
    main()
