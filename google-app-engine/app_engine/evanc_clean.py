import os
import numpy as np
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext.webapp import template
from doanc import ag,init
from datetime import date

p_sr = None
p_best = None
p_worst = None
w_best = None

class TestMapReduce: #(base_handler.PipelineBase):
    def run(self, filekey, blobkey):
        print "run!"
#        output = yield mapreduce_pipeline.MapreducePipeline(
#                                                            "change_the_object",
#                                                            "TestMapReduce.map_step",
#                                                            "TestMapReduce.reduce_step",
#                                                            "mapreduce.input_readers.BlobstoreLineInputReader",
#                                                            "mapreduce.output_writers.BlobstoreOutputWriter",
#                                                            mapper_params={
#                                                                           "blob_key": blobkey,
#                                                                           },
#                                                            reducer_params={
#                                                                            "mime_type": "text/plain",
#                                                                            },
#                                                            shards=16)
    def map_step(self):
        print "map step!"
        
    def reduce_step(self):
        print "reduce step!"

class WaitHandler(webapp.RequestHandler):
    def get(self):
        pipeline_id = self.request.get('pipeline')
        pipeline = 1 #mapreduce_pipeline.MapreducePipeline.from_id(pipeline_id)
        if pipeline.has_finalized:
            self.redirect('/done?pipeline=' + pipeline.pipeline_id)
        else:
            self.redirect('/wait?pipeline=' + pipeline.pipeline_id)

class DoneHandler(webapp.RequestHandler):
    def get(self):
        temp_array = np.arange(5)
        template_values = {
            'time': str(date.today()),
            'test_array' : str(temp_array)
        }
        path = os.path.join(os.path.dirname(__file__), 'index.html')
        self.response.out.write(template.render(path, template_values))
        
class MainHandler(webapp.RequestHandler):
    def get(self):
#        pipeline = mapreduce_pipeline.MapreducePipeline(TestMapReduce())
#        pipeline.start()
        self.redirect('done')
#         + pipeline.pipeline_id)

class InitHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write("This InitHandler is not for getting!")
        
    def post(self):
        global p_sr, p_best, p_worst, w_best
        M = int(self.request.get('M'))
        LP = int(self.request.get('LP'))
        LW = int(self.request.get('LW'))
        pc = float(self.request.get('pc'))
        pm = float(self.request.get('pm'))
        bits = int(self.request.get('bits'))
        limit = float(self.request.get('limit'))
        fp = float(self.request.get('fp'))
        signal_size = float(self.request.get('signal_size'))
        p_sr = [0 for x in range(0,M+1,1)]
        p_best = [0 for x in range(0,M+1,1)]
        p_worst = [0 for x in range(0,M+1,1)]
        w_best = [0 for x in range(0,M+1,1)]    
        init(M, LP, LW, pc, pm, bits, limit, fp, signal_size)
        s = "M = "+str(M)+"; LP = "+str(LP)+"; LW = "+str(LW)+"; pc = "+str(pc)+"; pm = "+str(pm)+"; bits = "+str(bits)+"; limit = "+str(limit)+"; fp = "+str(fp)+"; signal_size = "+str(signal_size)
        self.response.out.write(s)

class RunHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write("This RunHandler is not for getting!")
        
    def post(self):
        global p_sr, p_best, p_worst, w_best
        #if 0.0 in float(M, LP, LW, bits, limit, fp, signal_size):
        #    self.response.out.write("Algorithm not initialized!")
        #    return
        n = int(self.request.get('gen'))
        s = "gen = "+str(n)
        self.response.out.write(s)
        try:
            (p_sr[n],p_best[n],p_worst[n],tictoc,w_best[n]) = ag()
        except Exception as inst:
            self.response.out.write(inst)
        s = "; p_sr = "+str(p_sr[n])+"; p_best = "+str(p_best[n])+"; p_worst = "+str(p_worst[n])+"; tictoc = "+str(tictoc)+"; w_best = "+str(w_best[n])
        self.response.out.write(s)


application = webapp.WSGIApplication(
[('/', MainHandler), ('/run', RunHandler), ('/init', InitHandler), ('/wait', WaitHandler), ('/done', DoneHandler)]
, debug=True)

def main():
#    import sys
#    for attr in ('stdin', 'stdout', 'stderr'):
#        setattr(sys, attr, getattr(sys, '__%s__' % attr))
#    import pdb
#    pdb.set_trace()
    run_wsgi_app(application)

if __name__ == '__main__':
    main()
