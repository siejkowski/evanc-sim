from generation import random_population
from verification import linear_scaling
from reproduction import rws
from crossover import onepoint
from mutation import bitflop
from succesion import trivial
from objective import sum_of_ones

from time import clock

limit = 0
bits = 0
M = 0
LP = 0
LW = 0
pc = 0.0
pm = 0.0
fp = 0
signal_size = 0
R = None

def init(Mi, L_pi, L_zi, pci, pmi, dki, limiti, fpi, ssi):
    global M, LP, LW, pc, pm, bits, limit, fp, signal_size, R
    M = Mi 
    LP = L_pi
    LW = L_zi 
    pc = pci 
    pm = pmi
    bits = dki 
    limit = limiti 
    fp = fpi 
    signal_size = ssi    

    R = random_population.do(LP,LW,bits,0.9)

def ag():
    global LW, pc, pm, bits, limit, fp, signal_size, R
    
    tic = clock()
    p = linear_scaling.do(R,sum_of_ones.do);
    p_sr = sum(p)/float(len(p))
    p_best = max(p)
    p_worst = min(p)
    w_best = R[p.index(max(p))]
    Rp = rws.do(R,p)
    onepoint.do(Rp,pc)
    bitflop.do(Rp,pm)
    R = trivial.do(Rp)
    toc = clock()
    print "It took about: ",(toc-tic)
    return p_sr,p_best,p_worst,(toc-tic),w_best
    


def test():
    global M, LP, LW, pc, pm, bits, limit, fp, signal_size, R, p, p_sr, p_best, p_worst
    init(8, 100, 8, 0.999, 0.001, 12, 1.0, 44100, 500)
    #print M, LP, LW, pc, pm, bits, limit, fp, signal_size, R, p, p_sr, p_best, p_worst
    (p_sr,p_best,p_worst,tictoc,w_best) = ag()
    print p_sr,p_best,p_worst,tictoc,w_best

if __name__ == '__main__':
  test()
