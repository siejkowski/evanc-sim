'''
Created on 16-04-2012

@author: ksiejkow
'''
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

class MainHandler(webapp.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('Welcome! This is...')

application = webapp.WSGIApplication([('/', MainHandler)], debug=True)

def main():
    run_wsgi_app(application)

if __name__ == '__main__':
    main()
