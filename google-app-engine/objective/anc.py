def do(R,bits):
    w = [0 for x in range(bits)]
    for x in R:
        for i in range(0,len(R)):
            s = str(x[((i-1)*bits+1):(i*bits)]).strip("[]").replace(", ","")
            print s
            w[i] = 0 #int(s,2)
    return w

def test():
    Rs = range(0,2,1)
    Rs[0] = [0,0,0,0,0,0,0,0,0,0]
    Rs[1] = [1,1,1,1,1,1,1,1,1,1]
    print do(Rs,2)
    #~ bitflop.bitflop(Rs)
    #~ print Rs

if __name__ == '__main__':
  test()
