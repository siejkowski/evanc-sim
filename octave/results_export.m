function [] = results_export(mode="",algorithm="",timestamp="",...
                                                            filepath=0)
source("results_tools.m"); % load the tools functions

%% loading data
load("-mat-binary",get_metadata_path(algorithm,timestamp,filepath));
printf("metadata file loaded: %s\n",get_metadata_path(algorithm,...
                                                timestamp,filepath));
load("-mat-binary", get_datastruct_path(algorithm,timestamp,mode,...
                                        filepath));
printf("data file loaded: %s\n",get_datastruct_path(algorithm,...
                                            timestamp,mode,filepath));
%% end loading data

if strcmp(mode,"cdf-best") || strcmp(mode,"cdf-mean")
    assignin('base','cdf_data',data);
elseif strcmp(mode,"conv-best")
	assignin('base','conv_data',data);
elseif strcmp(mode,"worst")

elseif strcmp(mode,"all")

end
