function results = cmaes(results,d,N,num_iters)
    
    p = struct %% 
    %%%%% Parametry wspólne dla wszystkich przebiegów algorytmu
    sigma = 0.5;                 % coordinate wise standard deviation (step size)
    stopfitness = -10000000000;  % stop if fitness < stopfitness (minimization)
    % parametry określające selekcję
    lambda = 4+ floor(3*log(N));  % population size, offspring number
    mu = lambda/2;               % number of parents/points for recombination
    weights = log(mu+1/2)-log(1:mu)'; % muXone array for weighted recombination
    mu = floor(mu);        
    weights = weights/sum(weights);     % normalize recombination weights array
    mueff=sum(weights)^2/sum(weights.^2); % variance-effectiveness of sum w_i x_i - WHY NOT 1/...?
    
    % parametry określające adaptację
    cc = (4 + mueff/N) / (N+4 + 2*mueff/N); % time constant for cumulation for C
    cs = (mueff+2) / (N+mueff+5);  % t-const for cumulation for sigma control
    c1 = 2 / ((N+1.3)^2+mueff);    % learning rate for rank-one update of C
    cmu = min(1-c1, 2 * (mueff-2+1/mueff) / ((N+2)^2+mueff));  % and for rank-mu update
    damps = 1 + 2*max(0, sqrt((mueff-1)/(N+1))-1) + cs; % damping for sigma 
                                                        % usually close to 1
    % Initialize dynamic (internal) strategy parameters and constants
    pc = zeros(N,1); ps = zeros(N,1);   % evolution paths for C and sigma
    B = eye(N,N);                       % B defines the coordinate system
    D = ones(N,1);                      % diagonal D defines the scaling
    C = B * diag(D.^2) * B';            % covariance matrix C
    invsqrtC = B * diag(D.^-1) * B';    % C^-1/2 
    eigeneval = 0;                      % track update of B and D
    chiN=N^0.5*(1-1/(4*N)+1/(21*N^2));  % expectation of 
                                        %   ||N(0,I)|| == norm(randn(N,1))
    
    % inicjalizacja zmiennych przechowujących wyniki pośrednie
    arx = zeros(N,lambda);                                   
    arfitness = zeros(1,lambda);
        
    xmean = rand(N,1); % inicjacja algorytmu punktem losowym 
    k_cell = mat2cell(1*ones(1,lambda),1,ones(1,lambda));
    
    counteval = 1;
      
    while counteval <= num_iters
        
        T = clock;  
        for k=1:1:lambda
            arx(:,k) = xmean + sigma * B * (D .* randn(N,1)); % m + sig * Normal(0,C)
            arfitness(:,k) = cmaes_eval(d,arx(:,k));
        end %for
        % Sort by fitness and compute weighted mean into xmean
        [arfitness, arindex] = sort(arfitness);  % minimization?
        xold = xmean;
        xmean = arx(:,arindex(1:mu)) * weights;  % recombination, new mean value
        
        % Cumulation: Update evolution paths
        ps = (1-cs) * ps + sqrt(cs*(2-cs)*mueff) * invsqrtC * (xmean-xold) / sigma; 
        hsig = sum(ps.^2)/(1-(1-cs)^(2*counteval/lambda))/N < 2 + 4/(N+1);
        pc = (1-cc) * pc + hsig * sqrt(cc*(2-cc)*mueff) * (xmean-xold) / sigma; 
    
        % Adapt covariance matrix C
        artmp = (1/sigma) * (arx(:,arindex(1:mu)) - repmat(xold,1,mu));  % mu difference vectors
        C = (1-c1-cmu) * C + c1 * (pc * pc' + (1-hsig) * cc*(2-cc) * C) + cmu * artmp * diag(weights) * artmp';
    
        % Adapt step size sigma
        sigma = sigma * exp((cs/damps)*(norm(ps)/chiN - 1)); 
        
        % Update B and D from C
        if counteval - eigeneval > lambda/(c1+cmu)/N/10  % to achieve O(N^2)
            eigeneval = counteval;
            C = triu(C) + triu(C,1)'; % enforce symmetry
            [B,D] = eig(C);           % eigen decomposition, B==normalized eigenvectors
            D = sqrt(diag(D));        % D contains standard deviations now
            invsqrtC = B * diag(D.^-1) * B';
        end
        
        % Break, if fitness is good enough or condition exceeds 1e14, better termination methods are advisable 
        % if arfitness(1) < stopfitness || max(D) > 1e7 * min(D)
        %   break;
        % end
    
        % Output 
        % more off;  % turn pagination off in Octave
        %% disp([num2str(counteval) ': ' num2str(arfitness(1)) ' ' ... 
             %% num2str(sigma*sqrt(max(diag(C)))) ' ' ...
             %% num2str(max(D) / min(D)) ' ' etime(clock, T)]);
       
        results.best_fitness(counteval) = arfitness(1);  % przystosowanie najlepszego w danej iteracji 
        results.best_x(counteval,:) = arx(:,arindex(1)); % najlepszy w danej iteracji
        results.mean_fitness(counteval) = cmaes_eval(d,xmean);      % przystosowanie sredniej w danej iteracji
        results.mean_x(counteval,:) = xmean;                          % srednia w danej iteracji
        results.worst_fitness(counteval) = arfitness(end);          % przystosowanie najgorszego w danej iteracji
        results.worst_x(counteval,:) = arx(:,arindex(end));           % najgorszy w danej iteracji
        results.cmaes_ps(counteval,:) = ps;
        results.cmaes_pc(counteval,:) = pc;
        
        counteval = counteval+1;
    
    end % while, end generation loop

end %function
