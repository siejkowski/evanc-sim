function [] = all_at_once()

	p = generate_parameters;
	r = anc_simulation(p);
	plot_results(r);

end