function e=shared_filter_anc(signal,coeffs)
    x = zeros(1,length(signal)+length(coeffs));	
    e = zeros(1,length(signal)); 
    y = zeros(1,length(signal)); 
    [x,y,e] = doFiltering(signal,coeffs,x,y,e); 
end
