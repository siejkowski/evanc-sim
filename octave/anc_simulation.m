function r = anc_simulation(p)

	r = struct;

	c = create_cmaes_state(p.N);

    verification_length = 5*p.N;

	error_signal     = p.input_signal(1:verification_length)';
    control_signal = zeros(verification_length+p.N,1);
	control_signal_model = zeros(verification_length,1);

    starting_point = 2*verification_length + p.N;

    buffer = zeros(p.N,1);
      
    for offset = starting_point:verification_length:length(p.input_signal)

        iter = floor(offset / verification_length); % numer wywołania pętli

        input_signal = p.input_signal(offset-verification_length+1:offset)';

        coeff_number = get_coeff_number(iter, c.lambda);

        reconstructed_signal = reconstruct_primary_signal(error_signal, control_signal_model);

        out_signal = apply_control(reconstructed_signal, c.coeffs(:,coeff_number));

        control_signal = out_signal;% apply_secondary_path(out_signal, p.path_coeffs);

        control_signal_model = -out_signal; %apply_secondary_model(out_signal, p.model_coeffs);

    	error_signal = acoustic_superposition(input_signal, control_signal);

    	c.fitness(coeff_number) = calculate_fitness(error_signal(2*p.N:end));

    	if coeff_number == c.lambda

            if mod(c.generation_index,100) == 1 || mod(c.generation_index,100) == 5
                figure(c.generation_index);
                subplot (4, 1, 1);
                plot(out_signal);
                title('out_signal');
                subplot (4, 1, 2);
                plot(control_signal_model);
                title('control_signal_model');
                subplot (4, 1, 3);
                plot(error_signal);
                title('error signal');
                subplot (4, 1, 4);
                plot(reconstructed_signal);
                title('reconstructed_signal');
            end

            c = update_cmaes_state(c,iter);
            c = create_new_generation(c);
            r = log_results(r,c);
	    		
    	end % if

        %end %for

        % Break, if fitness is good enough or condition exceeds 1e14, better termination methods are advisable 
        % if fitness(1) < stopfitness || max(D) > 1e7 * min(D)
        %   break;
        % end
    
        % Output 
        % more off;  % turn pagination off in Octave
        %% disp([num2str(iter) ': ' num2str(fitness(1)) ' ' ... 
             %% num2str(sigma*sqrt(max(diag(C)))) ' ' ...
             %% num2str(max(D) / min(D)) ' ' etime(clock, T)]);
       
        % r.best_fitness(iter) = fitness(1);  % przystosowanie najlepszego w danej iteracji 
        % r.best_x(iter,:) = coeffs(:,coeffs_index(1)); % najlepszy w danej iteracji
        % r.mean_fitness(iter) = cmaes_eval(d,xmean);      % przystosowanie sredniej w danej iteracji
        % r.mean_x(iter,:) = xmean;                          % srednia w danej iteracji
        % r.worst_fitness(iter) = fitness(end);          % przystosowanie najgorszego w danej iteracji
        % r.worst_x(iter,:) = coeffs(:,coeffs_index(end));           % najgorszy w danej iteracji
        % r.cmaes_ps(iter,:) = ps;
        % r.cmaes_pc(iter,:) = pc;
    
    end % while, end generation loop

end % function


function c = create_cmaes_state(N)

	c = struct;

    c.N = N;

    c.generation_index = 0;

	%%%%% Parametry wspólne dla wszystkich przebiegów algorytmu
    c.sigma = 0.5;          % coordinate wise standard deviation (step size)
    c.stopfitness = -1e20;  % stop if fitness < stopfitness (minimization)
    
    % parametry określające selekcję
    c.lambda = 10 + floor(3*log(c.N));            % population size, offspring number
    c.mu = c.lambda/2;                          % number of parents/points for recombination
    c.weights = log(c.mu+1/2)-log(1:c.mu)'; 	% muXone array for weighted recombination
    c.mu = floor(c.mu);                     
    c.weights = c.weights/sum(c.weights);       % normalize recombination weights array
    c.mueff=sum(c.weights)^2/sum(c.weights.^2); % variance-effectiveness of sum w_i x_i - WHY NOT 1/...?

    % parametry określające adaptację
    c.cc = (4 + c.mueff/c.N) / (c.N+4 + 2*c.mueff/c.N); 	% time constant for cumulation for C
    c.cs = (c.mueff+2) / (c.N+c.mueff+5);  					% t-const for cumulation for sigma control
    c.c1 = 2 / ((c.N+1.3)^2+c.mueff);    					% learning rate for rank-one update of C
    c.cmu = min(1-c.c1, 2 * (c.mueff-2+1/c.mueff) / ((c.N+2)^2+c.mueff));  % and for rank-mu update
    c.damps = 1 + 2*max(0, sqrt((c.mueff-1)/(c.N+1))-1) + c.cs; % damping for sigma, usually close to 1

    % Initialize dynamic (internal) strategy parameters and constants
    c.pc = zeros(c.N,1); c.ps = zeros(c.N,1);   % evolution paths for C and sigma
    c.B = eye(c.N,c.N);                       	% B defines the coordinate system
    c.D = ones(c.N,1);                      	% diagonal D defines the scaling
    c.C = c.B * diag(c.D.^2) * c.B';            % covariance matrix C
    c.invsqrtC = c.B * diag(c.D.^-1) * c.B';    % C^-1/2 
    c.eigeneveal_iter = 0;                      		% track update of B and D
    c.chiN=N^0.5*(1-1/(4*c.N)+1/(21*c.N^2));  % expectation of 
                                        		%   ||N(0,I)|| == norm(randn(N,1))                                                      

    % inicjalizacja zmiennych przechowujących wyniki pośrednie
    c.coeffs = zeros(c.N,c.lambda);                                   
    c.fitness = zeros(1,c.lambda);
        
    % inicjacja algorytmu punktem losowym 
    c.xmean = rand(c.N,1); 
    c.xold = c.xmean;

    c = create_new_generation(c);

end % function

function c = update_cmaes_state(c,iter)

% Sort by fitness and compute weighted mean into xmean
    [c.fitness, c.coeffs_index] = sort(c.fitness,'ascend');  % minimization
    c.xold = c.xmean;
    c.xmean = c.coeffs(:,c.coeffs_index(1:c.mu)) * c.weights;  % recombination, new mean value

    % Cumulation: Update evolution paths
    c.ps = (1-c.cs) * c.ps + sqrt(c.cs*(2-c.cs)*c.mueff) * c.invsqrtC * (c.xmean-c.xold) / c.sigma; 
    c.hsig = sum(c.ps.^2)/(1-(1-c.cs)^(2*c.generation_index/c.lambda))/c.N < 2 + 4/(c.N+1);
    c.pc = (1-c.cc) * c.pc + c.hsig * sqrt(c.cc*(2-c.cc)*c.mueff) * (c.xmean-c.xold) / c.sigma; 

    % Adapt covariance matrix C
    artmp = (1/c.sigma) * (c.coeffs(:,c.coeffs_index(1:c.mu)) - repmat(c.xold,1,c.mu));  % mu difference vectors
    c.C = (1-c.c1-c.cmu) * c.C + c.c1 * (c.pc * c.pc' + (1-c.hsig) * c.cc *(2-c.cc) * c.C) + \
    	  c.cmu * artmp * diag(c.weights) * artmp';

    % Adapt step size sigma
    c.sigma = c.sigma * exp((c.cs/c.damps)*(norm(c.ps)/c.chiN - 1)); 
    
    % Update B and D from C
    if c.generation_index - c.eigeneveal_iter > c.lambda/(c.c1+c.cmu)/c.N/10  % to achieve O(N^2)
        c.eigeneveal_iter = c.generation_index;
        c.C = triu(c.C) + triu(c.C,1)'; % enforce symmetry
        [c.B,c.D] = eig(c.C);           % eigen decomposition, B==normalized eigenvectors
        c.D = sqrt(diag(c.D));        % D contains standard deviations now
        c.invsqrtC = c.B * diag(c.D.^-1) * c.B';
    end

end % function 

function c = create_new_generation(c)

    for k = 1:1:c.lambda
        c.coeffs(:,k) = c.xmean + c.sigma * c.B * (c.D .* randn(c.N,1)); % m + sig * Normal(0,C)
    end %for

    c.generation_index += 1;

end % function

function r = log_results(r,c)

    r.best_fitness(c.generation_index) = c.fitness(1);  % przystosowanie najlepszego w danej iteracji 
    r.best_x(:,c.generation_index) = c.coeffs(:,c.coeffs_index(1)); % najlepszy w danej iteracji

end % function

function coeff_number = get_coeff_number(iter,lambda)

	coeff_number = mod(iter, lambda);
    if coeff_number == 0
    	coeff_number = lambda;
    end %if

end % function
