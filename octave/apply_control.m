function out_signal = apply_control(reconstructed_signal, control_coeffs)
	
	out_signal = filter(control_coeffs, 1, reconstructed_signal);

end % function

%!test
%! A = ones(1,10); B = zeros(1,100); expected = zeros(1,10);
%! assert(apply_control(A,B), expected);

%!test
%! A = zeros(1,10); B = ones(1,100); expected = zeros(1,10);
%! assert(apply_control(A,B), expected);

%!test
%! A = ones(1,10); B = ones(1,100); expected = [1:10];
%! assert(apply_control(A,B), expected);

%!test
%! A = ones(1,10); B = ones(1,100);
%! apply_control(A,B);
%! clear all
%! A = zeros(1,10); B = ones(1,100); expected = zeros(1,10);
%! assert(apply_control(A,B), expected);