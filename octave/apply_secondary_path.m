function control_signal = apply_secondary_path(out_signal, path_coeffs)

	persistent filter_state

	filter_state_offset = 1;
	if length(path_coeffs)-filter_state_offset ~= length(filter_state)
		filter_state = zeros(length(path_coeffs)-filter_state_offset,1); % reinitialization of filter state
	end %if
	
	[control_signal, filter_state] = filter(path_coeffs, 1, out_signal, filter_state);

end %function

%!test
%! A = ones(1,10); B = zeros(1,100); expected = zeros(1,10);
%! assert(apply_secondary_path(A,B), expected);

%!test
%! A = zeros(1,10); B = ones(1,100); expected = zeros(1,10);
%! assert(apply_secondary_path(A,B), expected);

%!test
%! A = ones(1,10); B = ones(1,100); expected = [1:10];
%! assert(apply_secondary_path(A,B), expected);

%!test
%! A = ones(1,10); B = ones(1,100);
%! apply_secondary_path(A,B);
%! clear all
%! A = zeros(1,10); B = ones(1,100); expected = zeros(1,10);
%! assert(apply_secondary_path(A,B), expected);
