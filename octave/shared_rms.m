function r = shared_rms(a)
	r = sqrt(sum(a.^2)/length(a));
end
