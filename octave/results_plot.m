function results_plot(mode="",algorithm="",timestamp="",...
                       fig_num=1,filepath=0,compatibility=false())
    source("results_tools.m"); % load the tools functions
    
    if ishandle(fig_num)
        close(fig_num);
    end;
    %% loading data
    load("-mat-binary",get_metadata_path(algorithm,timestamp,filepath));
    printf("metadata file loaded: %s\n",get_metadata_path(algorithm,...
                                                   timestamp,filepath));
    load("-mat-binary", get_datastruct_path(algorithm,timestamp,mode,...
                                            filepath));
    printf("data file loaded: %s\n",get_datastruct_path(algorithm,...
                                              timestamp,mode,filepath));
    %% end loading data
    
    if compatibility==true()
        params.dims = N;
        params.num_dims = length(N);
        params.num_sigs = number_of_signals;
        params.num_stas = number_of_algorithm_starts;
        params.num_iters = number_of_algorithm_iters;
    end
    
    data_index = 1:1:size(data)(1);
    colors = ['b-';'r-';'k-';'g-';'m-';'c-'];
    figure(fig_num);
    legend();
    
    if strcmp(mode,"cdf-best") || strcmp(mode,"cdf-mean")
        plots_num = params.num_sigs;
        [dim,sig] = decombine_dim_sig_index(data_index, ...
                                                    params.num_sigs);
        [plots_cols,plots_rows] = plots_dim_from_plots_num(plots_num);
        % loop over cdf_data columns - nNsS data
        for i=data_index
            cdf_sort = sort(data(i,:));
            [plot_row,plot_col,plot_pos] = ...
                    row_col_pos_from_index_plots_num(i,plots_num);
            subplot(plots_cols,plots_rows,plot_pos);
            hold on;
            semilogx([10^0,cdf_sort(1),cdf_sort,10^10], ...
                    [0,(0:1:length(cdf_sort))/length(cdf_sort),1], ...
                    strcat(colors(dim(i)),";","n = ",...
                    num2str(params.dims(dim(i))),", s = ", ...
                    num2str(sig(i)),";"),'LineWidth',2);
            hold off;
        end;
        if strcmp(mode,"cdf-best")
            title('dystrybuanta empiryczna dla najlepszych');
            xlabel('przystosowanie najlepszego osobnika');
        elseif strcmp(mode,"cdf-mean")
            title('dystrybuanta empiryczna dla srednich');
            xlabel('przystosowanie najlepszego sredniego osobnika');
        end
        ylabel('znormalizowana czestosc');
        
    elseif strcmp(mode,"conv-best") || strcmp(mode,"conv-mean") ...
                                     || strcmp(mode,"conv-worst")
        plots_num = params.num_sigs*params.num_dims;
        [dim,sig] = decombine_dim_sig_sta_index(data_index, ... 
                        params.num_sigs,params.num_stas);
        [plots_cols,plots_rows] = plots_dim_from_plots_num(plots_num);
        % loop over data columns - nNsSrR data
        for i=data_index
            conv_data = data(i,:);
            plot_pos = ceil(i/params.num_stas);
            subplot(plots_cols,plots_rows,plot_pos);
            hold on;
            plot(1:1:length(conv_data),conv_data,colors(dim(i)), ...
                                                        'LineWidth',1);
            hold off;
        end;
        if strcmp(mode,"conv-best")
            title('krzywa zbieznosci dla najlepszych');
            xlabel('przystosowanie najlepszego osobnika');
        elseif strcmp(mode,"conv-mean")
            title('krzywa zbieznosci dla srednich');
            xlabel('przystosowanie sredniego osobnika');
        elseif strcmp(mode,"conv-worst")
            title('krzywa zbieznosci dla najgorszych');
            xlabel('przystosowanie najgorszego osobnika');
        end
        ylabel('numer iteracji algorytmu');
        
    elseif strcmp(mode,"atten-best") || strcmp(mode,"atten-mean")
        plots_num = params.num_sigs;
        [dim,sig] = decombine_dim_sig_index(data_index,params.num_sigs);
        [plots_cols,plots_rows] = plots_dim_from_plots_num(plots_num);
        % loop over cdf_data columns - nNsS data
        for i=data_index
            atten_sort = sort(data(i,:));
            [plot_row,plot_col,plot_pos] = ...
                    row_col_pos_from_index_plots_num(i,plots_num);
            subplot(plots_cols,plots_rows,plot_pos);
            hold on;
            hnew = bar([-300,atten_sort(1),atten_sort,100], ...
                   [0,(0:1:length(atten_sort))/length(atten_sort),1],...
                    "facecolor", colors(dim(i)), "edgecolor", ... 
                    colors(dim(i)));
            [LEGH,OBJH,OUTH,OUTM] = legend;
            legend([OUTH;hnew],OUTM{:},strcat("n = ", ...
                        num2str(N(dim(i))),", s = ",num2str(sig(i))));
            hold off;
        end;
        if strcmp(mode,"atten-best")
            title('dystrybuanta empiryczna dla tlumienia najlepszych');
            xlabel('tlumienie najlepszego osobnika');
        elseif strcmp(mode,"atten-mean")
            title('dystrybuanta empiryczna dla tlumienia srednich');
            xlabel('tlumienie sredniego osobnika');
        end
        ylabel('znormalizowana czestosc');
        
    end
    
    h_xlabel = get(gca,'XLabel');
    set(h_xlabel,'FontSize',14); 
    h_xlabel = get(gca,'YLabel');
    set(h_xlabel,'FontSize',14);
    h_xlabel = get(gca,'Title');
    set(h_xlabel,'FontSize',14);

end

%figure(2);
%subplot(4,1,1);
%plot(d(offset:end));
%title('d');
%ylabel('amplituda [.]');
%subplot(4,1,2);
%plot(x(offset:end));
%title('x');
%ylabel('amplituda [.]');
%subplot(4,1,3);
%plot(y(offset:end));
%title('y');
%ylabel('amplituda [.]');
%subplot(4,1,4);
%plot(e(offset:end));
%title('e');
%ylabel('amplituda [.]');
%xlabel('numer probki [-]');

%print('01.sygnaly.jpg','-djpg');

%% offset = length(h);
%% x = zeros(1,length(d)+offset);		% x - sygnał wejściowy filtru
%% e = zeros(1,length(d));			% 
%% y = zeros(1,length(d));			% 
%% 
%% [x,y,e] = shared_anc_system(d,h,x,y,e);
%% 
%% fft_size = 2048;
%% step = f_p/fft_size;
%% d_fft = fft((d)(end-fft_size:end),fft_size);
%% x_fft = fft((x)(end-fft_size:end),fft_size);
%% y_fft = fft((y)(end-fft_size:end),fft_size);
%% e_fft = fft((e)(end-fft_size:end),fft_size);
%% plot_start = 1;
%% plot_stop = 1000;
%% axe_x = plot_start:step:plot_stop;
%% 
%% figure(4);
%% %subplot(2,1,1);
%% %plot(axe_x,10*log10(abs(1/fft_size*d_fft(1:length(axe_x)))),'b',axe_x,10*log10(abs(1/fft_size*x_fft(1:length(axe_x)))),'r');
%% %title('widmo d vs widmo y');
%% %ylabel('poziom [dB]');
%% subplot(2,1,1);
%% plot(axe_x,10*log10(abs(1/fft_size*d_fft(1:length(axe_x)))),'b-','LineWidth',2,axe_x,10*log10(abs(1/fft_size*e_fft(1:length(axe_x)))),'r-','LineWidth',2);
%% title('widma sygnalow: wejsciowego i bledu');
%% ylabel('poziom sygnalu [dB]');
%% legend('wejsciowy','bledu');
%% h_xlabel = get(gca,'XLabel');
%% set(h_xlabel,'FontSize',14); 
%% h_xlabel = get(gca,'YLabel');
%% set(h_xlabel,'FontSize',14);
%% h_xlabel = get(gca,'Title');
%% set(h_xlabel,'FontSize',14);
%% subplot(2,1,2);
%% plot(axe_x,10*log10(abs(1/fft_size*e_fft(1:length(axe_x))))-10*log10(abs(1/fft_size*d_fft(1:length(axe_x)))),'k-','LineWidth',2);
%% title('redukcja');
%% ylabel('roznica poziomow [dB]');
%% xlabel('czestotliwosc [Hz]');
%% legend('redukcja');
%% h_xlabel = get(gca,'XLabel');
%% set(h_xlabel,'FontSize',14); 
%% h_xlabel = get(gca,'YLabel');
%% set(h_xlabel,'FontSize',14);
%% h_xlabel = get(gca,'Title');
%% set(h_xlabel,'FontSize',14);
%% 
%% print('widma_sygnalow.jpg','-djpg','-S640,640');
