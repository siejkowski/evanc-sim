## up-to-date (roadmap):

+ acoustic_superposition(primary_signal, control_signal => error_signal)
+ apply_secondary_path(out_signal, path_coeffs => control_signal)
+ apply_secondary_model(out_signal, model_coeffs => TESTS)
+ reconstruct_primary_signal(error_signal, out_signal_model => reconstructed_signal)
+ apply_control(reconstructed_signal, control_coeffs => out_signal)
+ calculate_fitness(error_signal => individual_fitness)


+[TESTS] update_control_coeffs(old_control_coeffs, generation_fitness => new_control_coeffs)

-[TESTS] anc_system_simulation()

-[TESTS] parameter generation functions!
-[TESTS] parameter getter functions!
-[TESTS] logging functions!
-[TESTS] MPI function (saves results to file as well)!



## deprecated:



ag_bin - skrypt startujący ag-bin, niezrównoleglony
-> z przedrostkiem ag_bin_ są wszystkie skrypty służące w symulacji ag_bin

anc - skrypt startujący symulacje, używa OpenMPI
-> z przedrostkiem anc_ są wszystkie skrypty służące do generowania parametrów

cmaes - skrypt startujący cmaesa, niezrównoleglony
-> z przedrostkiem cmaes_ są wszystkie skrypty służące w symulacji cmaes

cluster - skrypt służący do komunikacji z klastrem
-> przedrostek cluster_ gromadzi skrypty służące do połączenia, uploadu, downloadu z/do klastra

de - skrypt startujący de, niezrównoleglony
-> z przedrostkiem de_ są wszystkie skrypty służące w symulacji de

octave - skrypty do pracy z octave, wewnętrzne

results - skrypt przetwarzający wyniki symulacji, operuje na danych surowych, ściągniętych z serwera
-> przedrostek results_ gromadzi skrypty pomocne w przetwarzaniu wyników

shared - skrypty używane przez wiele algorytmów równocześnie

test - skrypt służący do testowania oraz pliki testowe (prototypy)
-> przedrostek test_ gromadzi takie pliki
