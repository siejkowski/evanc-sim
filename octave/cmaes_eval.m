function fitness=cmaes_eval(d,h)
	e = shared_filter_anc(d,h);
	fitness = shared_rms(e(length(h):end));
end
