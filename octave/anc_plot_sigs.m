function anc_plot_sigs(params=0)
    
    close all;
    
    if ~isstruct(params)
        params = struct;
        params = anc_gen_sim_params(params);
	    params = anc_gen_sig_params(params);
	    params = anc_gen_sigs(params);
    end
    
    figure(1); % przebiegi czasowe
    subplot(params.num_sigs,1,1);
    plot(1:1:params.signal_length_in_samples,params.sigs(1,:), ...
                                                    'k','LineWidth',2);
    subplot(params.num_sigs,1,2);
    plot(1:1:params.signal_length_in_samples,params.sigs(2,:), ...
                                                    'b','LineWidth',2);
    subplot(params.num_sigs,1,3);
    plot(1:1:params.signal_length_in_samples,params.sigs(3,:), ...
                                                    'r','LineWidth',2);
    subplot(params.num_sigs,1,4);
    plot(1:1:params.signal_length_in_samples,params.sigs(4,:), ...
                                                    'g','LineWidth',2);
    subplot(params.num_sigs,1,5);
    plot(1:1:params.signal_length_in_samples,params.sigs(5,:), ...
                                                    'm','LineWidth',2);
    fft_size = 2^floor(log2(params.signal_length_in_samples));
    d_fft = zeros(params.num_sigs,fft_size);
    for i=1:1:params.num_sigs
        d_fft(i,:) = fft((params.sigs(i,:)),fft_size);
    end
       
    plot_start = 1;
    plot_stop = 1000;
    step = params.probing_frequency_in_Hz/fft_size;
    freq_axis = plot_start:step:plot_stop;
    
    figure(2); % widma
    subplot(params.num_sigs,1,1);plot(freq_axis,10*log10(abs( ...
        1/fft_size*d_fft(1,1:length(freq_axis)))),'k-','LineWidth',2);
    subplot(params.num_sigs,1,2);plot(freq_axis,10*log10(abs( ...
        1/fft_size*d_fft(2,1:length(freq_axis)))),'b-','LineWidth',2);
    subplot(params.num_sigs,1,3);plot(freq_axis,10*log10(abs( ...
        1/fft_size*d_fft(3,1:length(freq_axis)))),'r-','LineWidth',2);
    subplot(params.num_sigs,1,4);plot(freq_axis,10*log10(abs( ...
        1/fft_size*d_fft(4,1:length(freq_axis)))),'g-','LineWidth',2);
    subplot(params.num_sigs,1,5);plot(freq_axis,10*log10(abs( ...
        1/fft_size*d_fft(5,1:length(freq_axis)))),'m-','LineWidth',2);
        
end
