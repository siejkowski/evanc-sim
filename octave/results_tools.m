function dim_sig_index = combine_dim_sig_to_index(dim,sig,num_sig)
%% combines two vectors dim and sig into one vector dim_sig_index
%% of length(dim_sig_index) = length(dim)*length(sig)
    dim_index = (kron(dim,ones(1,length(sig))).-1).*num_sig;
    sig_index = repmat(sig,1,length(dim));
    dim_sig_index = dim_index.+sig_index;
end

function [dim,sig] = decombine_dim_sig_index(dim_sig_index,num_sig)
%% return two vectors of length(dim_sig_index) with dim and sig for
%% every element in dim_sig_index
    dim = ceil(dim_sig_index./num_sig);
    sig = dim_sig_index.-(dim.-1).*num_sig;
end

function dim_sig_index = combine_dim_sig_sta_to_index(dim,sig,sta,...
                                                    num_sig,num_sta)
%% combines three vectors dim, sig and iter into one vector 
%% dim_sig_index of length(dim_sig_index) = length(dim)*length(sig)
    dim_index = (kron(dim,ones(1,length(sig)*length(sta))).-1).* ... 
                                                    (num_sig*num_sta);
    sig_index = repmat(((kron(sig,ones(1,length(sta))).-1).* ...
                                            num_sta),1,length(dim));
    sta_index = repmat(sta,1,length(dim)*length(sig));
    dim_sig_index = dim_index.+sig_index+sta_index;
end

function [dim,sig,sta] = ...
       decombine_dim_sig_sta_index(dim_sig_sta_index,num_sig,num_sta)
%% return threee vectors of length(dim_sig_index) with dim, sig and iter
%% for every element in dim_sig_index
    dim = ceil(dim_sig_sta_index./(num_sig*num_sta));
    sig = ceil((dim_sig_sta_index.-(dim.-1).*num_sig.*num_sta)./num_sta);
    sta = mod(dim_sig_sta_index,num_sta); 
    sta(sta==0)=num_sta;
end


function [plots_rows,plots_columns] = plots_dim_from_plots_num(plots_num)
   plots_columns = ceil(sqrt(plots_num));
   plots_rows = ceil(plots_num/plots_columns);
end

function [plot_row,plot_col,plot_pos] = ... 
                row_col_pos_from_index_plots_num(index,plots_num)
    [plot_rows,plot_columns] = plots_dim_from_plots_num(plots_num);
    index = mod(index,plots_num); if index==0, index = plots_num; end
    plot_row = ceil(index/plot_columns);
    plot_col = index-(plot_row-1)*plot_columns;
    plot_pos = index;
end

function datadirpath = fix_datadirpath(datadirpath=0)
    if ~ischar(datadirpath)
        datadirpath = ...
            "/home/ksiejkow/Dropbox/magisterka/octave/";
    end
end

function metadata_path = get_metadata_path(algorithm="", ...
                                            timestamp="",datadirpath=0)
    if ~ischar(algorithm) || ~ischar(timestamp)
        error("Expected strings for algorithm and timestamp ... 
parameters");
    end
    metadata_path = strcat(fix_datadirpath(datadirpath),"results/",...
                            algorithm,"-",timestamp,".dat");
end

function data_path = get_data_path(algorithm="",timestamp="",N=0,...
                                    d=0,r=0,datadirpath=0)
    if ~ischar(algorithm) || ~ischar(timestamp)
        error("Expected strings for algorithm and timestamp ... 
parameters");
    end
    if ~isnumeric(N) || ~isnumeric(d) || ~isnumeric(r)
        error("Expected numeric values for N, d and r parameters");
    end
    data_path = strcat(fix_datadirpath(datadirpath),"results/",...
                       algorithm,"-",timestamp,'-N',num2str(N),'-d', ...
							  num2str(d),'-r',num2str(r),".dat");
end

function datastruct_path = get_datastruct_path(algorithm="",...
                                    timestamp="",mode="",datadirpath=0)
    if ~ischar(algorithm) || ~ischar(timestamp) || ~ischar(mode)
        error("Expected strings for algorithm, timestamp and mode ...
parameters");
    end
    datastruct_path = strcat(fix_datadirpath(datadirpath),"results/",...
                       algorithm,"-",timestamp,"-",mode,".dat");
end

%!shared dim,sig,sta
%! disp("tests: result_tools.m");
%!assert(combine_dim_sig_to_index([1,2],[1,2],2),[1,2,3,4]);
%! [dim,sig] = decombine_dim_sig_index([1,2,3,4],2);
%!assert(dim,[1,1,2,2])
%!assert(sig,[1,2,1,2]);
%!assert(combine_dim_sig_to_index([3,5],[2,4],4),[10,12,18,20]);
%! [dim,sig] = decombine_dim_sig_index([10,12,18,20],4);
%!assert(dim,[3,3,5,5]);
%!assert(sig,[2,4,2,4]);
%!assert(combine_dim_sig_sta_to_index([1,2],[1,2],[1,2],2,2),[1,2,3,4,5,6,7,8]);
%! [dim,sig,sta] = decombine_dim_sig_sta_index([1,2,3,4,5,6,7,8],2,2);
%!assert(dim,[1,1,1,1,2,2,2,2]);
%!assert(sig,[1,1,2,2,1,1,2,2]);
%!assert(sta,[1,2,1,2,1,2,1,2]);
%!assert(combine_dim_sig_sta_to_index([2,4],[3,5],[1,3],5,3),[22,24,28,30,52,54,58,60]);
%! [dim,sig,sta] = decombine_dim_sig_sta_index([22,24,28,30,52,54,58,60],5,3);
%!assert(dim,[2,2,2,2,4,4,4,4]);
%!assert(sig,[3,3,5,5,3,3,5,5]);
%!assert(sta,[1,3,1,3,1,3,1,3]);

%!shared row,col,pos
%! [row,col] = plots_dim_from_plots_num(30);
%!assert(row,5);
%!assert(col,6);
%! [row,col] = plots_dim_from_plots_num(21);
%!assert(row,5);
%!assert(col,5);
%! [row,col,pos] = row_col_pos_from_index_plots_num(15,30);
%!assert(row,3);
%!assert(col,3);
%!assert(pos,15);
%! [row,col,pos] = row_col_pos_from_index_plots_num(15,21);
%!assert(row,3);
%!assert(col,5);
%!assert(pos,15);
%!assert(ischar(fix_datadirpath()),true);
%!assert(ischar(fix_datadirpath(132.2)),true);
%!assert(ischar(fix_datadirpath("")),true);
%!assert(ischar(get_metadata_path()),true);
%!error get_metadata_path(132.2,1233,123123,1212);
%!assert(ischar(get_data_path()),true);
%!error get_data_path("a","b","c",12,3);
%!assert(ischar(get_datastruct_path()),true)
%!error get_datastruct_path("a","b",123);
