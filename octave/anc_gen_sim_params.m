function params = anc_gen_sim_params(params)

    params.dims = [8];
    params.num_dims = length(params.dims);
	params.num_stas =  1;
    params.num_iters = 100;  
    
end %function

%!shared sim_params
%! sim_params = anc_gen_sim_params();
%!assert(isstruct(sim_params),true);
%!assert(isscalar(sim_params.iterations_number),true);
%!assert(isscalar(sim_params.starts_number),true);
%!assert(ismatrix(sim_params.dimensions),true);
