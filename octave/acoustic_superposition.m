function error_signal = acoustic_superposition(primary_signal, control_signal)

	error_signal = primary_signal + control_signal;

end % function

%!test
%! A = zeros(1,100); B = zeros(1,100); expected = zeros(1,100);
%! assert(acoustic_superposition(A,B), expected);

%!test
%! A = ones(1,100); B = zeros(1,100); expected = ones(1,100);
%! assert(acoustic_superposition(A,B), expected);

%!test
%! A = ones(1,100); B = ones(1,100); expected = 2*ones(1,100);
%! assert(acoustic_superposition(A,B), expected);

%!test
%! A = -1*ones(1,100); B = ones(1,100); expected = zeros(1,100);
%! assert(acoustic_superposition(A,B), expected);

%!shared A,B
%! A = zeros(1,10); B = zeros(1,100);
%!error <nonconformant arguments> acoustic_superposition(A,B) ("here the lengths of arguments don't match, expected error");

