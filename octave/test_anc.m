function test_anc()

	mlock()
	test_file("acoustic_superposition.m");
	test_file("apply_secondary_path.m");
	test_file("apply_secondary_model.m");
	test_file("reconstruct_primary_signal.m");
	test_file("apply_control.m");
	test_file("calculate_fitness.m")
	munlock()
    clear all;

end

function test_file(filename)
	source(filename);
	printf('%s \t - \t',filename);
	test(filename,'normal');
end
