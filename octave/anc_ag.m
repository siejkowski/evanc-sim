p_c_start = 0.001;
p_c_end = 0.01;
p_c_lpoints = 1;

p_m_start = 0.00005;
p_m_end = 0.0005;
p_m_lpoints = 1;

bits = 12;	% liczba bitów kodujących współczynnik ustalona na 12

%% blok inicjacji algorytmu - tutaj ustalamy parametry ag
global stopeval;
M = stopeval;	% stopeval - liczba generacji/pokoleń
LP = 100;	% LP - liczba osobników w populacji
p_c = linspace(p_c_start,p_c_end,p_c_lpoints);	% p_c - prawdopodobieństwo krzyżowania
p_m = linspace(p_m_start,p_m_end,p_m_lpoints);	% p_m - prawdopodobieństwo mutacji

for j=1:1:p_c_lpoints
	for k=1:1:p_m_lpoints

		%% blok wykonania algorytmu genetycznego
		[R,w,p] = ag(stopeval,LP,LW,bits,limit,p_c(j),p_m(k));

		% blok przetwarzania danych
		tic
		for i=1:1:stopeval
			inc = min(find(p(:,i)==max(p(:,i))));
			p_best(i) = p(inc,i);
			best(i,:) = w(inc,:);
			inc = min(find(p(:,i)==min(p(:,i))));
			p_worst(i) = p(inc,i);
			worst(i,:) = w(inc,:);
			p_avg(i) = mean(p(:,i));
		endfor;
		toc
		
		figure(1);
		subplot(p_c_lpoints,p_m_lpoints,(j-1)*p_m_lpoints+k);
		plot([p_best' p_avg' p_worst']);
		legend('najlepszy','srednia','najgorszy')
		xlabel('numer populacji [-]');
		ylabel('wartosc przystosowania [-]');
		title('Przystosowanie w danej iteracji');
		print('00.testowe.jpg','-djpg');
	endfor;
endfor;

h = best(end,:);

anc_plots;
