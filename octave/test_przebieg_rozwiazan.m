figure(2);
plot(1:1:length(out.dat),-out.dat,'b-','LineWidth',3);
%axis([10 10^5 -0.1 1.2]);
title('przebieg algorytmu');
legend('N=32');
ylabel('przystosowanie najlepszego osobnika');
xlabel('numer iteracji');
h_xlabel = get(gca,'XLabel');
set(h_xlabel,'FontSize',14); 
h_xlabel = get(gca,'YLabel');
set(h_xlabel,'FontSize',14);
h_xlabel = get(gca,'Title');
set(h_xlabel,'FontSize',14);


print('przebieg_rozwiazan.jpg','-djpg','-S640,480');
