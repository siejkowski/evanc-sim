d = rand(1,2200);
h = rand(1,128);

tic;
for i=1:1:1280
	de_eval(d,h);
end
toc;

tic;
for i=1:1:18
	cmaes_eval(d,h);
end
toc;


%!shared i,j
%! [i,j] = [i,j] = find(2*eye(2));
%!assert(i,[1;2])
%!assert(j,[1;2])

%%[best_sort_32,i] = sort(-1.*best_fitness(1:15));

%figure(1)
%semilogx([10,best_sort_32(1),best_sort_32,10^5],[0,(0:1:length(best_sort_32))/length(best_sort_32),1],'b-','LineWidth',3,[10,best_sort_64(1),best_sort_64,10^5],[0,(0:1:length(best_sort_64))/length(best_sort_64),1],'r-','LineWidth',3);
%axis([10 10^5 -0.1 1.2]);
%title('dystrybuanta empiryczna');
%legend('N=32','N=64');
%xlabel('przystosowanie najlepszego osobnika');
%ylabel('znormalizowana czestosc');
%h_xlabel = get(gca,'XLabel');
%set(h_xlabel,'FontSize',14); 
%h_xlabel = get(gca,'YLabel');
%set(h_xlabel,'FontSize',14);
%h_xlabel = get(gca,'Title');
%set(h_xlabel,'FontSize',14);


%print('dystrybuanta_empiryczna.jpg','-djpg','-S640,480');
