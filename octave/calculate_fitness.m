function individual_fitness = calculate_fitness(error_signal)

	individual_fitness = rms(error_signal);

end % function

function r = rms(a)
	r = abs(sqrt(sum(a.^2)/length(a)));
end % function


%!test
%! A = zeros(1,100); expected = 0;
%! assert(calculate_fitness(A), expected);

%!test
%! A = ones(1,100); expected = 1;
%! assert(calculate_fitness(A), expected);

%!test
%! A = ones(1,100); B = -1*A;
%! assert(calculate_fitness(A), calculate_fitness(B));

%!test
%! A = ones(1,50); B = -1*A; C = [A;B](:)'; D = [B;A](:)';
%! assert(calculate_fitness(C), calculate_fitness(D));
