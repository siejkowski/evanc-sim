function control_signal_model = apply_secondary_model(out_signal, model_coeffs)

	persistent filter_state

	filter_state_offset = 1;
	if length(model_coeffs)-filter_state_offset ~= length(filter_state)
		filter_state = zeros(length(model_coeffs)-filter_state_offset,1); % reinitialization of filter state
	end %if
	
	[control_signal_model, filter_state] = filter(model_coeffs, 1, out_signal, filter_state);

end %function

%!test
%! A = ones(1,10); B = zeros(1,100); expected = zeros(1,10);
%! assert(apply_secondary_model(A,B), expected);

%!test
%! A = zeros(1,10); B = ones(1,100); expected = zeros(1,10);
%! assert(apply_secondary_model(A,B), expected);

%!test
%! A = ones(1,10); B = ones(1,100); expected = [1:10];
%! assert(apply_secondary_model(A,B), expected);

%!test
%! A = ones(1,10); B = ones(1,100);
%! apply_secondary_model(A,B);
%! clear all
%! A = zeros(1,10); B = ones(1,100); expected = zeros(1,10);
%! assert(apply_secondary_model(A,B), expected);
