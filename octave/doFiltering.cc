#include <octave/oct.h>

double firfilter(RowVector x, RowVector h, octave_idx_type x_offset, octave_idx_type h_length); 

DEFUN_DLD(doFiltering, args, ,
          "System ANC. d,h,x,y,e") {
    RowVector d(args(0).matrix_value());   
    RowVector h(args(1).matrix_value());

    RowVector x(args(2).matrix_value());
    RowVector y(args(3).matrix_value());
    RowVector e(args(4).matrix_value());    

    octave_idx_type h_length = h.length();
    octave_idx_type d_length = d.length();

    // x(h_length-1) = 0; czyli x(0)-x(0+h_length-1)
    y(0) = firfilter(x,h,0,h_length);
    e(0) = d(0)+y(0);

    for (octave_idx_type i = 0; i < d_length-1; i++) {
		x(i+h_length) = e(i) - y(i); // x(h_length-1+1+i), czyli x(i+1)--h(i+1+h_length-1)
		y(i+1) = firfilter(x,h,i+1,h_length);
		e(i+1) = d(i+1)+y(i+1);
	//printf("i = %d, i+1 = %d, i+1+h_length = %d\n",i,i+1,i+1+h_length);
    }

    octave_value_list f_return;
    f_return(0) = x;
    f_return(1) = y;
    f_return(2) = e;
    return f_return;
}

double firfilter(RowVector x, RowVector h, octave_idx_type x_offset, octave_idx_type h_length) {
    double sum = 0;
    for(octave_idx_type j = 0; j < h_length; j++) {
		sum = sum + h(j)*x(h_length-1-j+x_offset); 
	// dla j=h_length-1 mamy x(x_offset)*h(h_length-1), czyli pierwszy*ostatni
	// dla j=0 mamy x(h_length-1+x_offset)*h(0), czyli ostatni*pierwszy
    }
    return sum;  
}
