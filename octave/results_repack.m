function [] = results_repack(mode="",algorithm="",timestamp="",...
                                      filepath=0,compatibility=false())

    source("results_tools.m"); % load the tools functions
    
    load("-mat-binary",get_metadata_path(algorithm,timestamp,filepath));
    printf("metadata file: %s\n",get_metadata_path(algorithm,...
                                                   timestamp,filepath));
                                                   
    if compatibility==true()
        params.dims = N;
        params.num_dims = length(N);
        params.num_sigs = number_of_signals;
        params.num_stas = number_of_algorithm_starts;
        params.num_iters = number_of_algorithm_iters;
    end
    
    % zainicjuj struktury do przechowywania danych
    if strcmp(mode,"cdf-best") || strcmp(mode,"cdf-mean") ... 
    || strcmp(mode,"atten-best") || strcmp(mode,"atten-mean")
        % data = | n1,s1,r1 n1,s2,r1 ... nn,ss,r1 |
        % 	     | n1,s1,r2 n1,s2,r2 ... nn,ss,r2 |
        data = zeros(params.num_dims*params.num_sigs, ...
                     params.num_stas);
    elseif strcmp(mode,"conv-best") || strcmp(mode,"conv-mean") ...
                                     || strcmp(mode,"conv-worst")
        % data = | n1,s1,r1,i1 n1,s1,r2,i1 ... nn,ss,rr,i1 |
        % 	     | n1,s1,r1,i2 n1,s1,r2,i2 ... nn,ss,rr,i2 |
        data = zeros(params.num_dims*params.num_sigs* ...
                     params.num_stas,params.num_iters);
    else
        error("unknown mode!");
    end 
    
    for dim=1:1:params.num_dims % po liczbie wymiarów
        for sig=1:1:params.num_sigs % po sygnałach testowych
            for sta=1:1:params.num_stas % po uruchomieniach algorytmu
                
                load("-mat-binary",get_data_path(algorithm,timestamp,...
                                    params.dims(dim),sig,sta,filepath));
                printf("data file loaded: %s\n",get_data_path(...
                        algorithm,timestamp,params.dims(dim), ...
                        sig,sta,filepath));
                            
                if compatibility==true()
                    params.sigs = results.params_d;
                end
                
                % określ, co chcesz wydobyć za dane
                if strcmp(mode,"cdf-best")
                    dim_sig_index = combine_dim_sig_to_index(dim,...
                                                sig,params.num_sigs);
                    data(dim_sig_index,sta) = ... 
                                           max(results.best_fitness(:));
                elseif strcmp(mode,"cdf-mean")
                    dim_sig_index = combine_dim_sig_to_index(dim,...
                                                sig,params.num_sigs);
                    data(dim_sig_index,sta) = ...
                                           max(results.mean_fitness(:));
                elseif strcmp(mode,"conv-best")
                    dim_sig_sta_index = ...
                        combine_dim_sig_sta_to_index(dim,sig,sta,...
                          params.num_sigs,params.num_stas);
                    data(dim_sig_sta_index,:) = ...
                                                results.best_fitness(:);
                elseif strcmp(mode,"conv-mean")
                    dim_sig_sta_index = ...
                        combine_dim_sig_sta_to_index(dim,sig,sta,...
                          params.num_sigs,params.num_stas);
                    data(dim_sig_sta_index,:) = ...
                                                results.mean_fitness(:);
                elseif strcmp(mode,"conv-worst")
                    dim_sig_sta_index = ...
                        combine_dim_sig_sta_to_index(dim,sig,sta,...
                          params.num_sigs,params.num_stas);
                    data(dim_sig_sta_index,:) = ...
                                               results.worst_fitness(:);
                elseif strcmp(mode,"atten-best") 
                    dim_sig_index = combine_dim_sig_to_index(dim,...
                                                sig,params.num_sigs);
                    [best_val,index] = max(results.best_fitness(:));
                    h = results.best_x(index,:);
                    d = params.sigs(sig);
                    e = shared_filter_anc(d,h);
                    rms_d = shared_rms(d(length(h):end));
                    rms_e = shared_rms(e(length(h):end));
                    data(dim_sig_index,sta) = 20*log10(rms_e/rms_d);
                elseif strcmp(mode,"atten-mean")
                    dim_sig_index = combine_dim_sig_to_index(dim,...
                                                sig,params.num_sigs);
                    [best_val,index] = max(results.mean_fitness(:));
                    h = results.mean_x(index,:);
                    d = params.sigs(sig);
                    e = shared_filter_anc(d,h);
                    rms_d = shared_rms(d(length(h):end));
                    rms_e = shared_rms(e(length(h):end));
                    atten = 20*log10(rms_e/rms_d);
                    printf("index: %d, sta: %d, atten: %f", ...
                                              dim_sig_index,sta,atten);
                    data(dim_sig_index,sta) = 20*log10(rms_e/rms_d);
                end 
                
            end % koniec pętli po uruchomieniach algorytmu
        end % koniec pętli po sygnałach testowych
    end % koniec pętli po liczbie wymiarów
    save("-mat-binary",get_datastruct_path(algorithm,timestamp,...
                                            mode,filepath),"data");
    printf("extracted data saved: %s",get_datastruct_path(algorithm,...
                                            timestamp,mode,filepath));
end											
%results_repack("cdf-best","cmaes","20120530T200733");
