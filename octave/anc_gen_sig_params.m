function params = anc_gen_sig_params(params)

    params.probing_frequency_in_Hz = 20000;
    params.signal_length_in_samples = params.probing_frequency_in_Hz/100;
    params.amplitude_in_units = 1.0; 

end %function

%!shared sig_params
%! sig_params = anc_gen_sig_params()
%!assert(isstruct(sig_params),true);
%!assert(isscalar(sig_params.probing_frequency_in_Hz),true);
%!assert(isscalar(sig_params.signal_length_in_samples),true);
%!assert(isscalar(sig_params.amplitude_in_units),true);
