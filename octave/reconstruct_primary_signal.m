function reconstructed_signal = reconstruct_primary_signal(error_signal, out_signal_model)

	reconstructed_signal = error_signal + out_signal_model;

end %function

%!test
%! A = zeros(1,100); B = zeros(1,100); expected = zeros(1,100);
%! assert(reconstruct_primary_signal(A,B), expected);

%!test
%! A = ones(1,100); B = zeros(1,100); expected = ones(1,100);
%! assert(reconstruct_primary_signal(A,B), expected);

%!test
%! A = ones(1,100); B = ones(1,100); expected = 2*ones(1,100);
%! assert(reconstruct_primary_signal(A,B), expected);

%!test
%! A = -1*ones(1,100); B = ones(1,100); expected = zeros(1,100);
%! assert(reconstruct_primary_signal(A,B), expected);

%!shared A,B
%! A = zeros(1,10); B = zeros(1,100);
%!error <nonconformant arguments> reconstruct_primary_signal(A,B) ("here the lengths of arguments don't match, expected error");

