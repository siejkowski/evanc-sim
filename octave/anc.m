%%mpirun -np 4 octave --eval "anc('cmaes')"

function info = anc()
	
	source("results_tools.m");
	pkg load openmpi_ext;
	MPI_Init;    
	MPI_COMM_WORLD=MPI_Comm_Load("EVANC");                   
	[rnk info] = MPI_Comm_rank (MPI_COMM_WORLD);
	[siz info] = MPI_Comm_size (MPI_COMM_WORLD);
	SLV = logical(rnk);
	MST = ~ SLV; 
	TAG1 = 101; TAG2 = 102; TAG3 = 103; TAG4 = 104;
	TAG5 = 105; TAG6 = 106; TAG7 = 107; TAG8 = 108;
	algorithm = "cmaes";
	params = create_params_container();
	if MST
	    if ~isdir("results"), mkdir("results"); end
	    vname=@(x) inputname(1);
		timestamp = datestr(now,30);
	    for (slv=1:1:(siz-1)) % pętla wysyłająca do slave'ów
	        info = MPI_Send(timestamp,slv,TAG7,MPI_COMM_WORLD);
	    end % for loop
	    save("-mat-binary",get_metadata_path(algorithm,timestamp,""),"params");
	    T = clock;
	else % SLV
	    [timestamp info] = MPI_Recv(MST,TAG7,MPI_COMM_WORLD);
	end % if MST
	
	disp(["MPI_Barrier - process rnk: " num2str(rnk)]);
	info = MPI_Barrier(MPI_COMM_WORLD);
	if SLV && info!=0
	    MPI_Finalize;
	end
	
	% pętla uruchomieniowa
	for t=(rnk+1):siz:params.total_num_stas
		[params.dim_num,params.sig_num,params.sta_num] = ... 
		 decombine_dim_sig_sta_index(t,params.num_sigs,params.num_stas);
	    params.dim = params.dims(params.dim_num); 
	    params.sig = params.sigs(params.sig_num,:);
	    %% create structure storing results
	    results = create_results_container(params);

	    results = cmaes(results,params.sig,params.dim, params.num_iters); 

	    save("-mat-binary",get_data_path(algorithm,timestamp, ...
				params.dim,params.sig_num,params.sta_num,""),"results")

	    if MST
			disp(["master - t: " num2str(t) ", siz: " num2str(siz) ... 
	              ", time: " num2str(etime(clock, T))]); T = clock;
	    else % SLV
			%~ disp(["slave - t: " num2str(t) ", rnk: " num2str(rnk)]);
	    end % if MST
	    clear results;
	end % pętli uruchomieniowej
	disp(["MPI_Barrier - process rnk: " num2str(rnk)]);
	info = MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize;
	disp(["MPI_Finalize - process rnk: " num2str(rnk)]);
	clear all;

end %function

function params = create_params_container()

	params = struct;
    params = anc_gen_sim_params(params);
    params = anc_gen_sig_params(params);
    params = anc_gen_sigs(params);
	params.total_num_stas = params.num_dims*params.num_sigs*params.num_stas;

end %function

function results = create_results_container(params)

	results = struct;
	results.best_fitness = zeros(params.num_iters,1); % przystosowanie najlepszego w danej iteracji 
    results.best_x = zeros(params.num_iters,params.dim);       % najlepszy w danej iteracji
    results.mean_fitness = zeros(params.num_iters,1); % przystosowanie sredniej w danej iteracji
    results.mean_x = zeros(params.num_iters,params.dim);       % srednia w danej iteracji
    results.worst_fitness = zeros(params.num_iters,1);% przystosowanie najgorszego w danej iteracji
    results.worst_x = zeros(params.num_iters,params.dim);      % najgorszy w danej iteracji
	results.cmaes_ps = zeros(params.num_iters,params.dim);
    results.cmaes_pc = zeros(params.num_iters,params.dim);

end %function
